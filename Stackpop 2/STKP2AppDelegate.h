//
//  STKP2AppDelegate.h
//  Stackpop 2
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GTMNSString+HTML.h"
#import "STKP2PreferencesDelegate.h"

@interface STKP2AppDelegate : NSObject <NSApplicationDelegate, NSUserNotificationCenterDelegate>
{
    NSDictionary *_primarySite;
    NSString *_accessToken;
}

@property (weak) IBOutlet NSMenu *menu;
@property (strong) STKP2PreferencesDelegate *preferencesDelegate;
@property NSStatusItem *statusItem;
@property NSString *accessToken;
@property NSString *primarySiteUrl;
@property NSString *primarySiteName;
@property NSTimer *loadTimer;
@property NSMutableArray *notificationsShown;
@property NSMutableArray *inboxItemsShown;
@property NSMutableArray *reputationChangesShown;
@property NSMutableArray *notificationTypes;
@property int timeStarted;

@end
